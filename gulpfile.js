'use strict';

// Include gulp
var gulp = require('gulp');

// Include Our Plugins
var minifyCSS = require('gulp-minify-css');
var livereload = require('gulp-livereload');
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');
var rename = require('gulp-rename');
var sass = require('gulp-sass')(require('sass'));
var prefix = require('gulp-autoprefixer');
var cssBase64 = require('gulp-css-base64');


gulp.task('sass', async function () {
    gulp.src('assets/styles/scss/*.scss')
        .pipe(sass().on('error', sass.logError))
		.pipe(prefix())
        .pipe(gulp.dest('assets/styles/css'));
});

// Watch Files For Changes
gulp.task('watch', function() {
    gulp.watch('assets/styles/scss/*.scss', gulp.series(['sass']));
});

// Default Task
gulp.task('default', gulp.series(['sass', 'watch']));
