function getFormObj(form) {
    var formObj = {};
    var inputs = form.serializeArray();
    jQuery.each(inputs, function (i, input) {
        formObj[input.name] = input.value;
    });
    return formObj;
}

jQuery(document)
    .on('submit', '#keyword-filter', function(e){
        e.preventDefault();
        let $form = jQuery(this).addClass('loading');;
        let formData = getFormObj($form);

        let data = { ...{
            action: keywordReplaceServerData.get_keyword_queried_posts_action,
            security: keywordReplaceServerData.security,
        } , ...formData};


        jQuery.ajax({
            url: keywordReplaceServerData.ajax_url,
            method: 'post',
            data: data,
        }).done(function(response) {
            $form.removeClass('loading');
            if(response.data && response.data !== '')
                jQuery('.table-replace-inner-holder').html(response.data.html);
        })
    })
    .on('submit', '[id^="replace-keyword-col"]', function(e){
        e.preventDefault();
        let $form = jQuery(this).addClass('loading');;
        let formData = getFormObj($form);

        let data = { ...{
            action: keywordReplaceServerData.replace_keyword_action,
            security: keywordReplaceServerData.security,
            keyword: jQuery('#keyword-filter [name="keyword"]').val(),
        } , ...formData};

        jQuery.ajax({
            url: keywordReplaceServerData.ajax_url,
            method: 'post',
            data: data,
        }).done(function(response) {
            $form.removeClass('loading');
            if(response.data && response.data !== '')
                jQuery('.table-replace-inner-holder').html(response.data.html);
        })

    });

