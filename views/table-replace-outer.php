<div id="table-replace-outer">
    <div class="wrap"><h2><?=__('Replace Key Word');?></h2></div>

    <form action="admin.php?page=replace-kw" id='keyword-filter'>
        <input type="text" placeholder="Keyword" name='keyword'/>
        <?php submit_button('Search'); ?>
    </form>
    
    <div class="table-replace-inner-holder">
        <?php include $tr_pl_dir . 'views/table-replace-inner.php'; ?>
    </div>
</div>