<?php 
namespace ReplaceKeyword;

class ReplacementTable extends \WP_List_Table {

    private $table_data;
    private $keyword;

    function get_columns()
    {
        $columns = array(
                'title'          => __('Title'),
                'content'         => __('Content'),
                '_yoast_wpseo_title'   => __('Meta Title'),
                '_yoast_wpseo_metadesc'        => __('Meta Description')
        );

        ob_start();
        submit_button('Replace');
        $subm = ob_get_clean();

        foreach($columns as $slug => $col) {
            
            $columns[$slug] .= "<form action='admin.php?page=replace-kw' id='replace-keyword-col-$slug'>
            <input type='text' placeholder='Replace with' name='replacement'/>
            <input type='hidden' name='column' value='$slug'/>
            $subm
        </form>";
        }
        return $columns;
    }

    private function symbol_as_tag($text, $symbol = '%%', $replace_odd = '[', $replace_even = ']'){

        if(!str_contains($text, $symbol)){
            return $text;
        }
    
        $text_parts = explode($symbol, $text);
        $new_text = $text_parts[0];
    
        foreach($text_parts as $i => $text_part){
            if(0 == $i){
                continue;
            }
    
            $new_text .= ($i%2 ? $replace_odd : $replace_even) . $text_part;
        }
    
        return $new_text;
        
    }

    function prepare_items($posts = [], $keyword = '')
    {
        $this->keyword = $keyword;
        $this->table_data = $this->format_table_data($posts);

        $columns = $this->get_columns();
        $hidden = array();
        $sortable = array();
        $this->_column_headers = array($columns, $hidden, $sortable);
        
        $this->items = $this->table_data;
    }

    private function format_table_data($queried_posts) {

        $queried_posts = array_map(function($p) {

            $format_a = [
                'title' => $p->post_title,
                'content' => $p->post_content,
                '_yoast_wpseo_title' => $this->symbol_as_tag(
                    empty($p->_yoast_wpseo_title)  ?
                    get_post_meta( $p->ID, '_yoast_wpseo_title',  true) :
                    $p->_yoast_wpseo_title
                ),
                '_yoast_wpseo_metadesc' => (empty($p->_yoast_wpseo_metadesc)  ? 
                    get_post_meta( $p->ID, '_yoast_wpseo_metadesc',  true) :
                    $p->_yoast_wpseo_metadesc),
            ];
    
    
            if(!empty($this->keyword)) {
                foreach( $format_a as $key => $val) {
                    $format_a[$key] =  preg_replace("/\b$this->keyword\b/i", "<span class='keword-higglight'>$this->keyword</span>", $val);
                }
            }

            return $format_a;
        }, $queried_posts);

        return $queried_posts;
    }

    function column_default($item, $column_name)
    {
          switch ($column_name) {
                case 'title':
                case 'content':
                case '_yoast_wpseo_title':
                case '_yoast_wpseo_metadesc':
                default:
                    return $item[$column_name];
          }
    }

}