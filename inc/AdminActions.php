<?php
namespace ReplaceKeyword;

class AdminActions {
    private $get_keyword_queried_posts_action = 'get_keyword_queried_posts';
    private $replace_keyword_action = 'replace_keyword';

    use AJAXHelper;

    function __construct() {
        add_action('admin_menu', [$this, 'add_update_replace_keyword_page']);
        add_action( 'admin_enqueue_scripts', [$this, 'enqueue_assets'] );
        add_action( 'wp_ajax_'.$this->get_keyword_queried_posts_action, [$this, 'get_keyword_queried_posts_cb'] );
        add_action( 'wp_ajax_'.$this->replace_keyword_action, [$this, 'replace_keyword_cb'] );
    }

    private function get_table_by_keyword($keyword) {
        global $wpdb, $tr_pl_dir;

        $karr = [
            $keyword,
            $keyword . ' %',
            '% ' . $keyword,
            '% ' . $keyword . ' %',
        ];

        $search_fields = ['p.post_title', ' p.post_content', 'q2.meta_value', 'q3.meta_value'];


        $queried_posts = $wpdb->get_results(
            $wpdb->prepare("
            SELECT 
                p.*, 
                q2.meta_value AS _yoast_wpseo_metadesc,
                q3.meta_value AS _yoast_wpseo_title
            FROM $wpdb->posts p 
            "
            .
                implode('', array_map(function($num){
                    return "
                    LEFT JOIN (
                        SELECT p{$num}.ID, m{$num}.meta_value 
                        FROM $wpdb->posts p{$num}
                        JOIN $wpdb->postmeta m{$num}
                            ON p{$num}.ID = m{$num}.post_id
                        WHERE
                            m{$num}.meta_key LIKE %s
                    ) AS q{$num}
                    ON
                        p.ID = q{$num}.ID
                    ";
                }, [2,3]))
            .
            "
            WHERE 
                p.post_type='post'
                AND (
                    "
                    . implode(
                        ' OR ', 
                        array_map(function($field){ 
                            return implode(' OR ', array_fill(0, 4, " {$field} LIKE %s "));
                        }, $search_fields)
                    ) . "
                   
                )
            ", '_yoast_wpseo_metadesc', '_yoast_wpseo_title', ...$karr, ...$karr, ...$karr, ...$karr)
        );

        $table = new ReplacementTable();
        $table->prepare_items($queried_posts, $keyword);

        ob_start();
        $table->display();
        $table_html = ob_get_clean();

        ob_start();
        include $tr_pl_dir . 'views/table-replace-inner.php';
        return ob_get_clean();
    }


    function replace_keyword_cb() {
        $this->check_AJAX_nonce();

        global $wpdb;

        $col = $_POST['column'];
        $rep = $_POST['replacement'];
        $keyword = $_POST['keyword'];

        switch($_POST['column']){
            case 'title':
            case 'content':
                $wpdb->query(
                    $wpdb->prepare("
                        UPDATE $wpdb->posts 
                        SET `post_{$col}` = REGEXP_REPLACE(`post_{$col}`,%s, %s) 
                        WHERE `post_{$col}` regexp %s
                        ",
                        "[[:<:]]{$keyword}[[:>:]]", $rep, "[[:<:]]{$keyword}[[:>:]]"
                    )
                );
                break;
            case '_yoast_wpseo_title':
            case '_yoast_wpseo_metadesc':
                $wpdb->query(
                    $wpdb->prepare("
                        UPDATE $wpdb->postmeta m
                        JOIN $wpdb->posts p
                        ON  m.post_id = p.ID
                        SET m.meta_value = REGEXP_REPLACE(m.meta_value,%s, %s) 
                        WHERE m.meta_key=%s
                        AND m.meta_value regexp %s
                        AND p.post_type='post'
                        ", "[[:<:]]{$keyword}[[:>:]]", $rep, $col, "[[:<:]]{$keyword}[[:>:]]"
                    )
                );
                break;
        }
        //echo  $wpdb->last_query; die;
        wp_send_json_success( ['html' => $this->get_table_by_keyword($keyword), 'lc' => $wpdb->last_query]);
    }


    function get_keyword_queried_posts_cb(){
        $this->check_AJAX_nonce();
        $keyword = $_POST['keyword'];
        wp_send_json_success( ['html' => $this->get_table_by_keyword($keyword)]);
    }

    function enqueue_assets(){
        global $tr_pl_dir_url;
        wp_register_script( 'replace-keyword-admin-js', $tr_pl_dir_url.'assets/scripts/admin.js', ['jquery'] );
        wp_localize_script( 'replace-keyword-admin-js', 'keywordReplaceServerData', [
            'security' => $this->get_AJAX_nonce(),
            'ajax_url' => $this->get_admin_ajax_url(),
            'get_keyword_queried_posts_action' => $this->get_keyword_queried_posts_action,
            'replace_keyword_action' => $this->replace_keyword_action,
        ]);
        wp_enqueue_script( 'replace-keyword-admin-js');

        wp_enqueue_style( 'replace-keyword-admin-css', $tr_pl_dir_url.'assets/styles/css/admin.css');
    }

    function add_update_replace_keyword_page(){
        add_menu_page(
            'Replace Keyword', //Page title
            'Replace Keyword', //Menu title
            'manage_options', //Capability
            'replace-kw', //menu slug
            [$this, 'replace_keywords_cb'],//callback
            '', // icon
            190,// priority
        );
    }
   

    function replace_keywords_cb() {
        global $tr_pl_dir;

        $query = new \WP_Query([
            'post_type' => 'post',
            'posts_per_page' => -1
        ]);

        $queried_posts = $query->get_posts();
        
        $table = new ReplacementTable();
        $table->prepare_items($queried_posts);

     

        include $tr_pl_dir . 'views/table-replace-outer.php';
    }
}

$AdminActions = new AdminActions;