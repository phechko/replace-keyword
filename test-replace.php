<?php 
/*
Plugin Name: Replace Keyword
Description: Replace Keyword
Version: 1.0.0
Author: pshechko <pshechko@outlook.com>
*/

$tr_pl_dir = plugin_dir_path(__FILE__);
$tr_pl_dir_url = plugin_dir_url(__FILE__);

require_once( ABSPATH . 'wp-admin/includes/screen.php' );
require_once( ABSPATH . 'wp-admin/includes/class-wp-list-table.php' );
require_once('inc/AJAXHelper.php');
require_once('inc/ReplacementTable.php');
require_once('inc/AdminActions.php');